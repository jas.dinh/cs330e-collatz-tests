#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)

    def test_read3(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)


    def test_eval_1(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_2(self):
        v = collatz_eval(2, 1)
        self.assertEqual(v, 2)

    def test_eval_3(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_4(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_5(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_6(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_7(self):
        v = collatz_eval(999910, 999999)
        self.assertEqual(v, 259)

    def test_solve_1(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_solve_2(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_solve_3(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 20, 100, 200)
        self.assertEqual(w.getvalue(), "20 100 200\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 0, 0, 0)
        self.assertEqual(w.getvalue(), "0 0 0\n")


    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n67 98\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n67 98 119\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("1 1\n2 1\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 1 2\n201 210 89\n900 1000 174\n")

    def test_solve3(self):
        r = StringIO("1 1\n2 1\n999910 999999\n903 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 1 2\n999910 999999 259\n903 1000 174\n")

    def test_solve4(self):
        r = StringIO("1 18\n29 153\n281 758\n9001 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 18 21\n29 153 122\n281 758 171\n9001 10000 260\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
